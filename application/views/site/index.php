<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Alê & André - 07 SET. 2013 - Villa Morena - RJ</title>
    <meta name="description" content="Site do casamento da Alê e André com informações sobre os noivos, padrinhos, RSVP, listra de presentes, local do evento, como chegar.">
  <meta name="keywords" content="site casamento, casório, andré, deco, alexandra, alê, ale, villa morena, vila morena, recreio dos bandeirantes, rsvp, jogo, site para casamento, skidun, rio de janeiro, rj"
    <meta name="author" content="Skidun">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/styles.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/flexslider.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/jquery.fancybox-1.3.4.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,200italic,300italic" rel="stylesheet" type="text/css">    
    <link href="<?php echo base_url();?>assets/css/bootstrap-responsive.css" rel="stylesheet">                    
    

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
                
        
  <script src="<?php echo base_url();?>assets/Scripts/swfobject_modified.js" type="text/javascript"></script>
</head>

    
    
  <body>
      
      
      <!-- HOME ############################################### -->
      <section id="home" >
          
          
          <div class="container">
              
              <div class="row ">
                  <div>
                      
                      <div id="logo"><img src="<?php echo base_url();?>assets/img/texto_bemvindo_capa.gif" alt="Logo" ></div>
                      
                      <div id="head" class="hidden-phone"></div>
                      
                      <img src="<?php echo base_url();?>assets/img/capa_felicidade.gif" style="margin-top:60px">
                      
                      <div id="arrow" class="hidden-phone"><a href="#historia"></a></div>
                      <div id="home_bg" class="hidden-phone hidden-tablet" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="0"></div>
                  </div>
              </div>
          </div>
          
      </section>
      <!-- /HOME ############################################### -->
      
      
      
      
      <!-- NAVBAR ############################################### -->
      <div class="nav-container hidden-phone">
          
          
          <nav>
              <div id="logo_small"><a href="#home"><img src="<?php echo base_url();?>assets/img/logo_andreeale.gif" alt="Logo small" ></a></div>
              <ul>
                  
                  <li><a href="#historia">Nossa história</a></li>
                  <li><a href="#pais">Aos nossos pais</a></li>
                  <li><a href="#gallery">Fotos</a></li>
                  <li><a href="#cerimonia">Cerimônia</a></li>
                  <li><a href="#recados">Recados</a></li>
                  <li><a href="http://sites.icasei.com.br/andreeale/pt_br/rsvp" target="_blank">Confirmação de presença</a></li>
				          <li><a href="https://sites.icasei.com.br/andreeale/pt_br/store/index/9" target="_blank">Presentes</a></li>
                  
                                    
              </ul>
          </nav>
          
          
      </div>
      <!-- /NAVBAR ############################################### -->
      
      <!-- NAVBAR Mobile############################################### -->
      <div class="container nav-mobile hidden-tablet hidden-desktop">
          
          
          
          
          <select class="dropmenu" name="dropmenu" onChange="$.scrollTo(this.value, 1500, {easing:'easeInOutExpo', axis:'y'})">
              <option value="" selected="selected">Menu</option>
              
              <option value="#home">Home</option>
              <option value="#historia">Nossa história</option>
              <option value="#pais">Aos nossos pais</option>
              <option value="#gallery">Fotos</option>
              <option value="#cerimonia">Cerimônia</option>
              <option value="#recados">Recados</option>
              <option value="#rsvp" rel="confirm">Confirmação de presença</option>
              <option value="#presentes">Presentes</option>
              
          </select>
          
          
          
      </div>
      <!-- /NAVBAR Mobile############################################### -->
      
      
      <!-- ABOUT ############################################### -->
      <section id="historia">
          
          <!-- Header
          <div class="header" >
          <div class="container visible-desktop" id="parallax" data-stellar-ratio="1.1" data-stellar-vertical-offset="0"></div>
          <div class="container visible-tablet" id="parallax_mobile" ></div>
          <div class="container visible-desktop" id="parallax_2" data-stellar-ratio="0.8" data-stellar-vertical-offset="50"></div>
          <div class="container visible-tablet" id="parallax_2_mobile" ></div>
          <div class="container visible-desktop" id="about_header" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="0"><h1>HISTÓRIA</h1></div>
          <div class="container visible-tablet" id="about_header_mobile" ><h1>ABOUT US</h1></div>
          </div>
 -->
          
          
          <!-- Content -->
          <div class="content">
              
              <div class="container">
                  
                  <div class="row">
                      
                      <div class="span12">
                          
                          <h2 class="centered">NOSSA HISTÓRIA</h2>
                          
                          <div class="row margin90">
                          
<h4 align="center">No dia 25 de dezembro de 2011 André pediu Alê em casamento. Ela disse SIM! <p><strong> E toda a nossa história começou assim...</strong></p></h4>

<br>


                          
                              <div class="span7"><img src="<?php echo base_url();?>assets/img/titulo_versao_ale.gif"><p>Era um domingo de setembro de 2009, eu e minha irmã Fernanda caminhávamos pela orla do Recreio em busca de um rede de vôlei que pudéssemos entrar para jogar. Depois de sermos rejeitadas por uma rede onde só jogavam coroas, encontramos essa tal rede no Posto 9, onde fomos recebidas com muito carinho pelas pessoas que ali estavam. Ah, essa rede! Foi ali onde tudo começou, pelo menos pra mim. Me lembro até hoje, ele estava de sunga preta, rsrsrsrs. E foi assim que ele começou a ser chamado depois daquele dia, "o moreno da sunga preta!".  Naquele dia estava um pouco cansada e de meias palavras, pois tinha saído no dia anterior. Nem chegamos a conversar nesse dia e inclusive fiquei sabendo depois que ele tentou puxar assunto comigo e eu não respondi, ficando com fama de metida já no primeiro dia. Mal sabe ele que eu não respondi porque não ouvi ele falando mesmo. <p>

O vôlei acabou e cada um foi pra um canto. No domingo seguinte fui com a expectativa de encontra-lo, mas infelizmente ele não tinha ido.<p>

Mais um domingo se passou e era dia de vôlei, mas dessa vez só a minha irmã tinha ido. Eu fiquei ajudando minha mãe com a obra no apartamento, pois estávamos nos mudando. Daqui a pouco a Fernanda me liga e diz: adivinha quem perguntou por você?! O moreno da sunga preta!! Kkkkkkk. Aproveitei a deixa que minha mãe tinha pedido pra eu comprar comida pra ela na rua e fui lá "buscar a Fernanda", rsrs. Chegando lá, resolvi jogar uma partida e ai os primeiros papos surgiram.<p>

Chegando em casa, fui direto para o Orkut ver se conseguia acha-lo por lá.... putz, ele estava na última página. Enfim, consegui adicioná-lo. Kkkkkkk, aaah, essas redes sociais! <p>

Conversa vai, conversa vem... vi que ele dançava forró e disse que gostaria muito de aprender a dançar. E foi logo a brechinha pra um indireta da parte dele - "ué, se você me ensinar a jogar vôlei, eu te ensino a dançar!".<p>

No dia 28 de setembro fomos para o forró do Barraco, ao som do nosso querido amigo Marco Vivan. Ele começou a me ensinar a dançar e no meio da música quis me ensinar um passo onde dançávamos de rosto colado, rsrs. Aí nem precisa falar, né?¹ Rolou o primeiro beijo! <p>

Depois desse dia continuamos a sair juntos e 2 meses depois ele pediu para namorar. No dia 25/12/2011 noivamos e agora estamos preparando nosso casamento, que será maravilhosooooo! Esperamos vocês lá.
</p>
                              </div>
                              
                              <div class="span4 offset1">
                                  <img src="<?php echo base_url();?>assets/img/foto_historia_ale_2.jpg" alt="img" >
                                  <img src="<?php echo base_url();?>assets/img/foto_historia_ale_1.jpg" alt="img" >
                                  <img src="<?php echo base_url();?>assets/img/foto_historia_ale_3.jpg" alt="img" >
                                  <img src="<?php echo base_url();?>assets/img/foto_historia_ale_5.jpg" alt="img" >                              </div>
                              
                          </div>
                          
                      </div>
                      
                      
						<div class="span12">
                          <hr>
                          
                          <div class="row margin90">
                              <div class="span7"><img src="<?php echo base_url();?>assets/img/titulo_versao_andre.gif"><p>Sim, definitivamente, as palavras têm poder. - "Se eu pego essa morena eu caso com ela!" E foi assim que tudo começou, numa tarde de domingo de sol, na praia do Recreio, numa troca de palavras com um amigo, referente àquela linda menina chamada Alê. Sim, claro, com um amigo, porque naquele dia, ela sequer olhou pra mim.<p>

Estava rolando aquela partida sagrada de vôlei com os amigos quando chegaram duas meninas lindas no calcadão e perguntaram para um dos jogadores: "ei, será que rola da gente jogar aí com vocês?". "Demorou" - eu pensei - "gente nova no pedaço, vamos ver qual é". Diga-se de passagem, até que as duas jogavam muito bem para a idade que tinham.<p>

Fim da partida. Vai começar mais uma. Ela de um lado, eu do outro, 5 metros nos separando. Partida vai, partida vem, eu filmando de cima em baixo cada detalhe daquela morena que jogava de calça de moleton num sol de rachar de 40º. Ponto para o time adversário. Num piscar de olhos, eis que a tal morena resolve tirar a calça e jogar mais à vontade, só de biquini. Isso, você leu certo. Senti uma maldade da outra parte. Meu coração parou por alguns segundos. Quase que matou o papai de tesão emoção! Foi amor à primeira vista. Quer dizer, segunda, terceira, quarta e sucessivas vistas. Haja vista!<p>

Domingo seguinte, crente que ia encontrar minha morena, nada feito. Foi só mais um jogo.<p>

Uma semana depois, chego na rede e avisto de longe a Fernandinha. Coração bateu mais forte na expectativa de encontrar a tal morena. Chego perto e lanço: "e aí, Fê, vamos jogar? cadê sua irmã?". Nada feito. Ela não viria nesse dia.<p>

Partida rolando, eis que me surje absolutamente do nada aquela morena linda e começa a jogar com a gente. Cheio de sorrisos, puxando assunto, dando tchau, buzinando ao sair de carro. Pensei "aí tem!"<p>

Cheguei em casa, liguei o computador e quem estava lá me adicionando? Alexandra Venetillo.<p>

A partir daí foi um pulo: um chat aqui, um forró ali, um passinho mal intencionado e eu estava mais próximo daquela que seria a mulher da minha vida, mãe dos meus filhos e que passarei todos os momentos felizes da minha vida! 
</p>
                              </div>
                              
                              <div class="span4 offset1">
                                  <img src="<?php echo base_url();?>assets/img/foto_historia_andre_1.jpg" alt="img" >
                                  <img src="<?php echo base_url();?>assets/img/foto_historia_andre_2.jpg" alt="img" >
                                  <img src="<?php echo base_url();?>assets/img/foto_historia_andre_4.jpg" alt="img" >
								  <img src="<?php echo base_url();?>assets/img/foto_historia_andre_5.jpg" alt="img" >

                                              </div>
                              
                          </div>
                          
                      </div>                      
                      
                 
 
                  </div>
                  
              </div><!-- /container -->
          </div><!-- /content -->
          
          
      </section>
      <!-- /ABOUT ############################################### -->
      


      <!-- PAIS ############################################### -->
      <section id="pais">
        
          
          <!-- Content -->
          <div class="content">
              
              <div class="container">
                                        
          <div class="span12 centered">
                          <img class="pais" src="<?php echo base_url();?>assets/img/img-sep-title.jpg" alt="">
                          <hr>

                          <h2>aos nossos pais</h2>


                            
                      </div>
                  
      <div class="span12">
        <div class="row margin90">
          <div class="span3">
            <img src="<?php echo base_url();?>assets/img/foto_pais_alexandre.jpg" alt="img" >
          </div>
          <div class="span3">
            <img src="<?php echo base_url();?>assets/img/foto_pais_christina.jpg" alt="img" >
          </div>
          <div class="span3">
            <img src="<?php echo base_url();?>assets/img/foto_pais_miguel.jpg" alt="img" >
          </div>
          <div class="span3">
            <img src="<?php echo base_url();?>assets/img/foto_pais_sandra.jpg" alt="img" >
          </div> 
          <div class="span12 centered ale_andre">
            <div class="row margin90">
              <div class="span4 ale">
               <img src="<?php echo base_url();?>assets/img/ale_love_pais.png" alt="img" >
              </div>
              <div class="span4 love">
                <img src="<?php echo base_url();?>assets/img/love_pais.png" alt="img" >
              </div>
              <div class="span3 and">
                <img src="<?php echo base_url();?>assets/img/andre_love_pais.png" alt="img" >
              </div>
               <p class="texto_love">A vocês, que nos deram a vida e nos ensinaram a vivê-la com dignidade, não bastaria um obrigado. 
A vocês, que iluminaram os caminhos obscuros com afeto e dedicação para que os trilhássemos sem medo e cheios de esperanças, não bastaria um muito obrigado. A vocês, que se doaram inteiros e renunciaram aos seus sonhos, para que, muitas vezes, pudéssemos realizar os nossos. Pela longa espera e compreensão durante nossas longas viagens, não bastaria um muitíssimo obrigado. <br><br>

A vocês, pais por natureza, por opção e amor, não bastaria dizer, que não temos palavras para agradecer tudo isso. Mas é o que nos acontece agora, quando procuramos arduamente uma forma verbal de exprimir uma emoção ímpar. Uma emoção que jamais seria traduzida por palavras.

<br><br>

<span>AMAMOS VOCÊS!</span>
</p>
            </div>
          </div>                      
         
          </div>
      </div>


      



     

              </div><!-- /container -->
          </div><!-- /content -->
          
          
      </section>
      <!-- /PAIS ############################################### -->      



     <!-- GALLERY ###############################################  -->
      <section id="gallery" >
          
          
          <div class="container">
              
              <div class="row">
                  
                  
                  <div class="span12" id="gallery_items">
                      
                      <h1 class="centered">FOTOS</h1>
                      
                      <div class="row margin90">
                          
                          <div class="span4">
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_1.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_1.jpg" alt="img" ></a></figure>
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_2.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_2.jpg" alt="img" ></a></figure>
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_3.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_3.jpg" alt="img" ></a></figure>
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_10.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_10.jpg" alt="img" ></a></figure>
                          </div>
                          <div class="span4">
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_4.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_4.jpg" alt="img" ></a></figure>
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_5.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_5.jpg" alt="img" ></a></figure>
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_6.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_6.jpg" alt="img" ></a></figure>
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_11.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_11.jpg" alt="img" ></a></figure>
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_12.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_12.jpg" alt="img" ></a></figure>
                          </div>
                          <div class="span4">
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_7.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_7.jpg" alt="img" ></a></figure>
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_8.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_8.jpg" alt="img" ></a></figure>
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_9.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_9.jpg" alt="img" ></a></figure>
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_13.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_13.jpg" alt="img" ></a></figure>
                              <figure><a class="enlarge fancybox" href="<?php echo base_url();?>assets/img/foto_galeria_14.jpg"><img src="<?php echo base_url();?>assets/img/foto_galeria_14.jpg" alt="img" ></a></figure>
                          </div>
                          
                          
                      </div>
                      
                  </div>
                  
                  <div id="gallery_bg" class="hidden-phone hidden-tablet" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="-2000"></div>
              </div>
              
          </div>
          
          
      </section>
      
      
      <!-- /GALLERY ############################################### -->

      
      
      <!-- SERVICES ############################################### -->
      <section id="cerimonia" >
          
          <!-- Header
          
          <div class="header" >
          <div class="container visible-desktop" id="parallax_3"  data-stellar-ratio="0.9" data-stellar-vertical-offset="-500" ></div>
          <div class="container visible-tablet" id="parallax_3_mobile" ></div>
          <div class="container visible-desktop" id="parallax_4" data-stellar-ratio="0.8" data-stellar-vertical-offset="200"></div>
          <div class="container visible-tablet" id="parallax_4_mobile" ></div>
          <div class="container visible-desktop" id="services_header" data-stellar-background-ratio="0.7" data-stellar-vertical-offset="0"><h1>Services</h1></div>
          <div class="container visible-tablet" id="services_header_mobile" ><h1>Services</h1></div>
          </div>
          
          <!-- /Header -->
          
          
          <!-- Content -->
          <div class="content">
              <div class="container">
                  
                  <div class="row">
                      
                      
                      <div class="span12 centered">
                          <img class="cerimonia" src="<?php echo base_url();?>assets/img/img-sep-title.jpg" alt="">
                          <hr>

                          <h2>Cerimônia e recepção</h2>


						                
                      </div>
                      
                      
                      
                      <!-- Services list -->
                      <div class="span12 margin90 services_list">
                      
                                                <h4 align="center">Não se preocupe. Vamos te dar toda orientação para sair da cerimônia e chegar na festa. É bem simples, veja: basta dar 10 passos e pronto! Cerimônia e festa serão no mesmo local, tudo para deixar a sua noite ainda mais confortável e prazerosa ;-)</h4>    
            
 
                     <!-- Process -->
                      <div class="span12">
                          <div class="row margin90" id="process">
                              <div class="span3 centered">
                                  <img src="<?php echo base_url();?>assets/img/process1.png" alt="img" >
                                  <h3>Quando?</h3>
                                  <p>07 de<br> setembro<br> de 2013,<br> feriado.</p>         
                              </div>
                              
                              <div class="span3 centered">
                                  <img src="<?php echo base_url();?>assets/img/process2.png" alt="img" >
                                  <h3>Que horas?</h3>
                                  <p>início da<br> cerimômina<br> às 19h.</p>
                              </div>
                              
                              <div class="span3 centered">
                                  <img src="<?php echo base_url();?>assets/img/process3.png" alt="img" >
                                  <h3>Onde?</h3>
                                  <p>Villa Morena:<br> Estrada do<br> Pontal, 1381.<br> Recreio dos<br> Bandeirantes.<br> Rio de Janeiro.</p>
                              </div>
                              
                              <div class="span3 centered">
                                  <img src="<?php echo base_url();?>assets/img/process4.png" alt="img" >
                                  <h3>Como chegar?</h3>
                                  <p>clique para ver<br> no mapa abaixo.</p>
                                  <a class="bottom-map" href="#map" ></a>
                              </div>
                                                        
                          </div>
                          
                      </div>
                     <!--  <div class="forcinha">
                      </div>   -->

                      <div class="span12 centered">
                         

                          <h2>Jogo da memória</h2>
                          <h4 align="center">Divirta-se encontrando os pares de imagens e aproveite para memorizar a data e ver mais fotos nossas.</h4>    


                            
                      </div>
                      <!-- /Process -->
 

<!-- JOGO ############################################### -->


<div align="center">
      <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="950" height="600" id="FlashID" title="jogo-aleeandre">
        <param name="movie" value="<?php echo base_url();?>assets/flash/jogo-aleeandre.swf">
        <param name="quality" value="high">
        <param name="wmode" value="opaque">
        <param name="swfversion" value="6.0.65.0">
        <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don't want users to see the prompt. -->
        <param name="expressinstall" value="<?php echo base_url();?>assets/Scripts/expressInstall.swf">
        <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
        <!--[if !IE]>-->
        <object type="application/x-shockwave-flash" data="<?php echo base_url();?>assets/flash/jogo-aleeandre.swf" width="950" height="600">
          <!--<![endif]-->
          <param name="quality" value="high">
          <param name="wmode" value="opaque">
          <param name="swfversion" value="6.0.65.0">
          <param name="expressinstall" value="<?php echo base_url();?>assets/Scripts/expressInstall.swf">
          <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
          <div>
            <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
            <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" width="112" height="33" /></a></p>
          </div>
          <!--[if !IE]>-->
        </object>
        <!--<![endif]-->
      </object>
</div>

<!-- /JOGO ############################################### --> 
 
 
 
                      
                      
 
                  
                  </div> 
                     <div class="span12 centered">
                         

                          <h2>como chegar?</h2>
                          <h4 align="center">Navegue pelo mapa e veja o melhor caminho para chegar ao local a partir de sua casa.</h4>    


                            
                      </div>
               </div><!-- /container -->
              
          </div><!-- /content -->
          
      </section>
    
      
          <!-- Mapa -->
          <div class="container" id="map"></div>

          <!-- /Mapa -->       
      
      <!-- /SERVICES ############################################### -->
  
      
      



      
 
      
      <!-- START A PROJECT ############################################### -->
      
      <section id="start_project">
          <div class="container" >
              <div class="row">
                  <div class="span12 centered">
                      <h2>Anote na agenda!</h2>
                      <h1>07.09.2013</a></h1>
                      
                      <!-- <p>Muitíssimo obrigado por ter visitado o site do nosso casamento. Nos encontramos daqui a pouquinho em Setembro! Até lá!<br>

Com muito amor,
Alê e André
</p> -->

                      
                  </div>
                  
              </div>
          </div>
      </section>
      
      <!-- /START A PROJECT ############################################### -->
      
      
      <!-- MAP ############################################### -->
      
      <section id="recados">
          <!-- Arqui vão ficar os recados --> 
          <div class="container" >
              <div class="row">
                   <div class="span12 centered">
                          <img class="rec" src="<?php echo base_url();?>assets/img/img-sep-title.jpg" alt="">
                          <hr>

                          <h2>Recados aos noivos</h2>


                            
                      </div>
                      <!-- [início] Recados -->
                      
                        <div class="acordion_recados">
                          <h3></h3>
                          <form action="home/save_recado" class="contato" id="form-recado" method="post">
                            <div class="span6">
                              <div class="campos left"> <!-- <span>&nbsp;</span> -->
                                <input type="text" id="nome_recado" name="nome_recado" value="Seu Nome" onfocus="if (this.value == 'Seu Nome') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Seu Nome';}">
                              </div>
                            </div>
                            <div class="span6">
                              <div class="campos right"> <!-- <span>&nbsp;</span> -->
                                <input type="text" id="email_recado" name="email_recado" value="Seu Email" onfocus="if (this.value == 'Seu Email') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Seu Email';}">
                              </div>
                            </div>
                            <div class="span12">  
                              <div class="texto"> <!-- <span>&nbsp;</span> -->
                                <label for="msgRecado"></label>
                                <textarea name="msg_recado" id="msg_recado" cols="45" rows="5"></textarea>
                                <input type="submit" id="executar" name="executar" class="envio" />
                                <img src="<?php echo base_url();?>assets/img/ajax-loader.gif" class="loader-recados"/>
                              </div>
                             </div> 
                          </form>
                          <div class="sucesso-recado"></div>
                        </div>
                        <div id="total-recados">
                          <span><?php echo $quantidade;?> </span><?php if($quantidade <= 1){echo ' amigo';}else{echo ' amigos';}?> já <?php if($quantidade <= 1){echo ' deixou';}else{echo ' deixaram';}?> recado. Confira:
                        </div>
                        <?php 
                          echo $recados;
                        ?>    
                  </div>       
              </div>
          </div>
      </section>
      <!-- /MAP ############################################### -->
      
            
      <!-- FOOTER ############################################### -->
      
      <section id="footer">
          <div class="container" >
              <div class="row">
                  <div class="span12 centered">
                    <p>
Muitíssimo obrigado por ter visitado o site do nosso casamento.<br> Nos encontramos daqui a pouquinho em Setembro! Até lá!
Com muito amor, Alê e André</p>
                      <p>FIM</p>
                      
                  </div>
                  
              </div>
          </div>
      </section>
      
      <!-- /FOOTER ############################################### -->
      
      
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2390687-22', 'aleeandre.com.br');
  ga('send', 'pageview');
swfobject.registerObject("FlashID");
swfobject.registerObject("FlashID");
</script>      
      
</body>
    
    
   <script src="<?php echo base_url();?>assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.easing.1.3.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.scrollTo.js"></script>
    <script src="<?php echo base_url();?>assets/js/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/navbar.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.stellar.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.flexslider.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.fancybox-1.3.4.pack.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.videoBG.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.gmap.js"></script>
    <!--<script src="<?php echo base_url();?>assets/js/jquery-ui-1.8.18.custom.min.js"></script>-->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    
    
    <script type="text/javascript">
        $(function(){
          $.stellar({
                    horizontalScrolling: false,
                    scrollProperty: 'scroll',
                    positionProperty: 'position',
                    verticalOffset: 0,
                    hideDistantElements: true,
                    viewportDetectionInterval: 100
                    });
          
          });
    </script>
    
        
    <script type="text/javascript">
        $(window).load(function() {
                       $('#portfolio1, #portfolio2, #portfolio3').flexslider({slideshow: false});
                       });
        </script>
    
     <script type="text/javascript">
        $(document).ready(function(){
                          $("#map").gMap({ controls: true,
                                         scrollwheel: false,
                                         draggable: true,
                                         markers: [{ latitude: -23.019827,
                                                   longitude: -43.520901,
                                                   icon: { image: "<?php echo base_url();?>assets/img/pin.png",
                                                   iconsize: [32, 47],
                                                   iconanchor: [32,47],
                                                   infowindowanchor: [12, 0] } }
                                                   ],
                                         icon: { image: "<?php echo base_url();?>assets/img/pin.png",
                                         iconsize: [32, 46],
                                         iconanchor: [12, 46],
                                         infowindowanchor: [12, 0] },
                                         latitude: -23.019827,
                                         longitude: -43.520901,
                                         zoom: 13 });
                          });
        </script>

</html>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<script src="<?php echo base_url(); ?>assets/admin/js/main.js"></script>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
</style>
</head>

<body>
	<div class="container">
      <div class="header">
        <ul class="nav nav-pills pull-right">
          <li <?php if($this->uri->segment(2)=='index' || $this->uri->segment(2)==''){echo 'class="active"';}?>><a href="<?php echo base_url();?>admin/">Recados</a></li>
          <li <?php if($this->uri->segment(2)=='usuarios'){echo 'class="active"';}?>><a href="<?php echo base_url();?>admin/usuarios">Usuários</a></li>
          <li><a href="<?php echo base_url();?>admin/usuarios/edit/<?php echo $this->session->userdata('id');?>"><?php echo $this->session->userdata('nome');?></a></li>
          <li><a href="<?php echo base_url();?>login/logout">Sair</a></li>
        </ul>
        <h3 class="text-muted">Admin - Alê e André</h3>
      </div>

      <div class="row">
      	<?php echo $output; ?>
      </div>

      <div class="footer">
        <p>© Skidun 2013</p>
      </div>

    </div> <!-- /container -->
</body>
</html>
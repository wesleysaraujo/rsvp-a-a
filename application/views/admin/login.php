<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <title>Admin - Alê & André</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/admin/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="<?php echo base_url();?>assets/admin/css/main.css" rel="stylesheet" media="screen">
  </head>
  <body>
  	<div class="painel-login">
    	<h1>Seja bem vindo!</h1>
    	<div class="panel <?php if($already_installed){echo 'panel-info';}else{echo 'panel-danger';}?>">
    		<div class="panel-heading"><?php if($already_installed){ echo 'Login de acesso';}else{echo 'Instalação da aplicação';}?></div>
    		<div class="panel-body">
    			<form class="form" role="form" action="<?php if($already_installed){ echo base_url()."login/validate";}else{echo base_url()."install/run";}?>" method="post">
					<?php if($this->session->flashdata('pass_recovery')):?><small class="esqueceu"><?php echo $this->session->flashdata('pass_recovery');?></small><?php endif;?>
					<div class="form-group">
						<label for="exampleInputEmail1">E-mail de login</label>
						<input type="text" name="txt-login" class="form-control" value="" size="" />
					</div>
					<div class="form-group">	
						<label for="exampleInputEmail1">Senha</label>
						<input type="password" name="txt-password" class="form-control" value="" id="senha-login" size="" />
					</div>	
					<div class="enviada-erro" <?php if(isset($error) || $this->session->flashdata('erro-login')){echo 'style="display:block;"';}?>><?php if(isset($error) || $this->session->flashdata('erro-login')){echo "E-mail ou senha informados estão incorretos.";}?></div>
					<div class="form-group">
						<input type="submit" class="btn <?php if($already_installed){echo 'btn-info';}else{echo 'btn-danger';}?>" name="entrar" value="Logar" size="" />
					</div>
				</form>
			</div>	
    	</div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/admin/js/bootstrap.min.js"></script>

    <!-- Enable responsive features in IE8 with Respond.js (https://github.com/scottjehl/Respond) -->
    <!-- <script src="<?php //echo base_url();?>assets/js/respond.js"></script> -->
  </body>
</html>
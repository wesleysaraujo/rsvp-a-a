<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recado_model extends CI_Model {

	private $table = 'recados';

	public function get()
    {
        $this->db->where('status', 'aprovado');
        return  $this->db->get($this->table);         
    }

	public function count_rows()
    {
    	$this->db->where('status', 'aprovado');
        return $this->db->count_all_results($this->table);
    }
	
	public function create($data)
	{

		return $this->db->insert($this->table, $data);
	
	}
	

}

/* End of file modelName.php */
/* Location: ./application/models/modelName.php */
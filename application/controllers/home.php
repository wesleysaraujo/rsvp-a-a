<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('recado_model');

		$recados = $this->recado_model->get();  

		$html_recado = '';
		$count = 1;
		foreach($recados->result() as $recado){
			$count++;
			if($count%2 == 0){
				$html_recado .= '<div class="mensagem c-claro">';
			}else{
				$html_recado .= '<div class="mensagem c-escuro">';
			}
			$html_recado .= '
                              <h1>'.$recado->nome.'</h1>
                              <p> " '.$recado->recado.' " </p>
                            </div>';
		}

		$data['recados'] = $html_recado;
		$data['quantidade'] = $this->recado_model->count_rows();
		$this->load->view('site/index', $data);
	}

	public function save_recado()
	{
		$this->load->model('recado_model');

		$data['nome'] 			= 	$this->input->get('nome_recado');
		$data['email'] 			= 	$this->input->get('email_recado');
		$data['recado'] 		= 	$this->input->get('msg_recado');
		$data['status']			=	'pendente';
		$data['date_create'] 	= 	date('Y-m-d');

		$create = $this->recado_model->create($data);

		if($create){
			echo "Recado cadastrado com sucesso";
		}else{
			echo "Falha ao cadastrar o recado";
		}


	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata('logado')){
			redirect('login');
		}
		$this->load->library('grocery_CRUD');
		$this->load->model('user_model', 'recado_model');
	}

	public function _admin_output($output = null)
	{
		$this->load->view('admin/admin.php',$output);
	}

	public function index()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('twitter-bootstrap');
			$crud->set_table('recados');
			$crud->set_subject('Recados');
			$crud->columns('nome','email','status','date_update');
			$crud->fields('nome', 'email', 'recado', 'status');

			$output = $crud->render();

			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function md5()
	{
		$md5 = md5('skidun08');	

		echo $md5;
	}

	public function encrypt_insert($post)
	{
		$pass = $this->input->get('senha');
		$post['senha'] = md5($pass);
		return $post;
	}

	public function encrypt_update($post)
	{
		$pass 	= $this->input->post('senha');
		$id 	= $this->uri->segment(4);
		$post['senha'] = md5($pass);
		return $post;
	}

	function encrypt_password_callback($post_array, $primary_key = null)
	{
	  $this->load->library('encrypt');
	  $key = 'super-secret-key';
	  $post_array['senha'] = $this->encrypt->encode($post_array['senha'], $key);
	  return $post_array;
	}

	public function usuarios()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('twitter-bootstrap');
			$crud->set_table('usuarios');
			$crud->set_subject('Usuários');
			$crud->columns('name','email', 'date_update');
			$crud->fields('name','email', 'senha');
			$crud->change_field_type('senha', 'password');
			//$crud->edit_fields('name', 'email');
			$crud->display_as('date_update','Data de atualização');

			//This Callback Encryption
            $crud->callback_before_insert(array($this,'encrypt_insert'));
            $crud->callback_before_update(array($this,'encrypt_update'));
			$output = $crud->render();

			$this->_admin_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
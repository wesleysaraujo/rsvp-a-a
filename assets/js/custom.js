$(document).ready(function(){

$('.bottom-map').bind('click',function(event){
     $('html, body').stop().animate({
     scrollTop: $('#map').offset().top-220
     }, 1200,'easeInOutQuint');

     event.preventDefault();
});


$('.dropmenu').change(function(){
    
     if(this.value == '#rsvp')
     {
          top.location.href ='http://sites.icasei.com.br/andreeale/pt_br/rsvp';
          
     }
      if(this.value == '#presentes')
     {
          top.location.href ='https://sites.icasei.com.br/andreeale/pt_br/store/index/9';
          
     }
});





//this function is used to position elements on the homepage
function sniffer() {
var windowHeight=$(window).height();
var introHeight=$(window).height()/10;
var windowWidth=$(window).width()/2;
var windowWidth2=windowWidth-900 ;


var home=$("#home");
home.css("height",windowHeight+"px");

var intro=$("#intro");
intro.css("margin-top",introHeight+"px");                                    

var logo=$("#logo");
logo.css("padding-top",introHeight+"px");

var arrow=$("#arrow");
arrow.css("left",windowWidth-40+"px");

var dropmenu=$(".nav-mobile");
dropmenu.css("width",$(window).width()-40+"px");
                  
}
sniffer();


//Accordion
/*
$('.acordion_recados').accordion({
	collapsible: true,
	active: false,
	autoHeight: false,
	icons: { 'header': 'expandir', 'headerSelected': 'contrair' } 
});
*/
//cadastrar recado
$('#executar').click(function(e){
     e.preventDefault();
     if($('#nome_recado').val == '' || $('#nome_recado').val == 'SEU NOME' || $('#email_recado').val() == '' || $('#email_recado').val() == 'SEU EMAIL' || $('#msg_recado').val() == ''){
          alert('Preencha todos os campos');
     }else{
          var action =  $('#form-recado').attr('action');
          $(this).hide();
          $('.loader-recados').fadeIn();
          var campos = $('#form-recado').serialize();
          console.log(campos);
          $.ajax({
               url: action,
               async: false,
               data: campos,
               success: function(retorno){
                    $('.sucesso-recado').html(retorno);
                    $('.sucesso-recado').delay(3000).html('');
                    $('.loader-recados').hide();
                    $('#nome_recado').val('Seu Nome');
                    $('#email_recado').val('Seu Email');
                    $('#msg_recado').val('');
                    $('#executar').fadeIn();
                    
               },
               error: function(){
                    $('.sucesso-recado').html(retorno);
                    $('.loader').fadeOut();
                    $('#executar').fadeIn();
               }
          });
     } 
});
//this function is used display the vidoe on the homepage
function video() {

var videoHeight=$(window).height();

$('#home_bg').videoBG({
                    mp4:'assets/video/videobg.mp4',
                    ogv:'assets/video/videobg.ogv',
                    webm:'assets/video/videobg.webm',
                    poster:'assets/video/videobg.jpg',
                    scale:true, 
                    });	

}
video();
                  
//fancybox settings
$("a.fancybox").fancybox({
overlayColor:'#7ca5b7',
overlayOpacity: 0.2,
});
                  
//on window resize event
$(window).resize(function(){
               sniffer();
               // video();
               });
});